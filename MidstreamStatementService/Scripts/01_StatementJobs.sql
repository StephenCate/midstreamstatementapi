﻿USE [Midstream]
GO

/****** Object:  Table [dbo].[StatementJobs]    Script Date: 04/20/2017 5:16:53 AM ******/
DROP TABLE [dbo].[StatementJobs]
GO

/****** Object:  Table [dbo].[StatementJobs]    Script Date: 04/20/2017 5:16:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StatementJobs](
	[Id] [uniqueidentifier] NOT NULL,
	[ClientCodes] [nvarchar](50) NOT NULL,
	[Period] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_StatementJobs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


