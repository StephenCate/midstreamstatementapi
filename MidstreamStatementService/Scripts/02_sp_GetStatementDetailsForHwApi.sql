﻿USE [Midstream]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetStatementDetailsForHwApi]    Script Date: 4/26/2017 12:41:00 PM ******/
DROP PROCEDURE [dbo].[sp_GetStatementDetailsForHwApi]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetStatementDetailsForHwApi]    Script Date: 4/26/2017 12:41:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Stephen Cate
-- Create date: April 20, 2017
-- Description:	Pulls all the Midstream DB based on Hw's API Requirement
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStatementDetailsForHwApi] 
	-- Add the parameters for the stored procedure here
	@ClientCodes NVARCHAR(100), 
	@Period INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
--DECLARE @ClientCodes NVARCHAR(100) = 'FEWV,FOH' -- csv of clientCodes in dbo.PRG_Programs based on dbo.USR_Users
--DECLARE @Period INT = 2 -- 1: January, 2: February, etc..

	SELECT s.* INTO #TempSearsApiData
	from (
	SELECT  Retailers.Id 'Account', 
			'FEE5DIST' 'AccountProvider',
			Retailers.RetailerName 'FullName', 
			Retailers.Email, 
			Retailers.CorporateAddress 'ServiceAddress', 
			Retailers.City 'ServiceCity',
			Retailers.[State] 'ServiceState', 
			Retailers.Zip 'ServiceZip',
			Retailers.PhoneNumber 'Phone',
			'' 'CustomerNotes',
			STR_Stores.ProviderCode 'CustomerProvider',
			Statements.Period 'CustomerInvoiceDescription', -- TODO: Map this to the new field Ryan will be creating
			V.prod_category 'MeasureProductType',
			V.tot_prd_qt 'MeasureQuantity',
			V.brand	'MeasureBrand',
			V.manf_id 'MeasureModelNumber',
			V.item_no 'MeasureSku',
			'' 'MeasureMef', -- TODO: Map this to Product.Mef
			'' 'MeasureCapacity', -- TODO: Map this to Product.Capacity
			V.prod_id 'MeasureEnergyStarModelNumber',
			STR_Stores.store_description 'MeasureStoreName',
			STR_Stores.city 'MeasureLocation',
			'FEE3NEXT' 'RebateAccountID',
			Retailers.RetailerName 'RebatePayeeName', 
			Retailers.CorporateAddress 'RebatePayeeAddress', 
			Retailers.City 'RebatePayeeCity',
			Retailers.[State] 'RebatePayeeState', 
			Retailers.Zip 'RebatePayeeZip',
			Retailers.PhoneNumber 'RebatePayeePhone',
			'PAID' 'RebateStatus',
			P.budget_per_energy_saved*tot_prd_qt 'RebateAmount',
			RetailerInvoices.CheckNumber 'RebatePaymentNumber',
			GETDATE() 'RebateStatusDate',
			1 'RebateApproved',
			1 'RebateCommited',
			P.budget_per_energy_saved*tot_prd_qt 'RebateCheckAmount',
			Statements.CreatedDate 'ApplicationStatusDate', -- TODO: Change to Statements.Statusonce Ryan update the field to be a date
			'Complete' 'ApplicationDescription',
			'Application Complete' 'ApplicationReason'
	FROM PRG_Programs P
		JOIN vw_POS_Matches V ON P.product_categories = V.prod_category
			AND MONTH(trs_dt) = @Period and YEAR(trs_dt)=2017
			AND tot_prd_qt > 0
			AND es_nes = 'ES'
		JOIN STR_Stores on STR_Stores.unit_number = REPLACE(V.store_no, '000','') AND STR_Stores.ClientCode = P.ClientCode
		JOIN Retailers on Retailers.RetailerName = 'Sears'
		JOIN Statements on Statements.Program_Id = @Period and Statements.ClientCode = P.ClientCode
		JOIN RetailerInvoices on Retailers.Id = RetailerInvoices.RetailerId 
			AND RetailerInvoices.StatementId = Statements.Id
	WHERE P.ClientCode IN (SELECT val FROM dbo.f_split(@ClientCodes,',')) 
	) s
	
	SELECT r.* INTO #TempHomeApiData
	from (
	SELECT Retailers.Id 'Account', 
			'FEE5DIST' 'AccountProvider',
			Retailers.RetailerName 'FullName', 
			Retailers.Email, 
			Retailers.CorporateAddress 'ServiceAddress', 
			Retailers.City 'ServiceCity',
			Retailers.[State] 'ServiceState', 
			Retailers.Zip 'ServiceZip',
			Retailers.PhoneNumber 'Phone',
			'' 'CustomerNotes',
			STR_Stores.ProviderCode 'CustomerProvider',
			Statements.Period 'CustomerInvoiceDescription', -- TODO: Map this to the new field Ryan will be creating
			HOME_DEPOT_POS.Prod_categ 'MeasureProductType',
			HOME_DEPOT_POS.NET_UNT_SLS 'MeasureQuantity',
			HOME_DEPOT_POS.Brand	'MeasureBrand',
			HOME_DEPOT_POS.Model 'MeasureModelNumber',
			HOME_DEPOT_POS.SKU_NBR 'MeasureSku',
			'' 'MeasureMef', -- TODO: Map this to Product.Mef
			'' 'MeasureCapacity', -- TODO: Map this to Product.Capacity
			HOME_DEPOT_POS.EnergyStar 'MeasureEnergyStarModelNumber',
			STR_Stores.store_description 'MeasureStoreName',
			STR_Stores.city 'MeasureLocation',
			'FEE3NEXT' 'RebateAccountID',
			Retailers.RetailerName 'RebatePayeeName', 
			Retailers.CorporateAddress 'RebatePayeeAddress', 
			Retailers.City 'RebatePayeeCity',
			Retailers.[State] 'RebatePayeeState', 
			Retailers.Zip 'RebatePayeeZip',
			Retailers.PhoneNumber 'RebatePayeePhone',
			'PAID' 'RebateStatus',
			P.budget_per_energy_saved*HOME_DEPOT_POS.NET_UNT_SLS 'RebateAmount',
			RetailerInvoices.CheckNumber 'RebatePaymentNumber',
			GETDATE() 'RebateStatusDate',
			1 'RebateApproved',
			1 'RebateCommited',
			P.budget_per_energy_saved*HOME_DEPOT_POS.NET_UNT_SLS 'RebateCheckAmount',
			Statements.CreatedDate 'ApplicationStatusDate', -- TODO: Change to Statements.Statusonce Ryan update the field to be a date
			'Complete' 'ApplicationDescription',
			'Application Complete' 'ApplicationReason'

		--'HOME DEPOT', P.program_name, ISNULL(SUM(NET_UNT_SLS),0) AS ECOUNT, p.budget_per_energy_saved,ISNULL(SUM(NET_UNT_SLS),0) * p.budget_per_energy_saved as incentive 
		FROM PRG_Programs P
		LEFT JOIN HOME_DEPOT_POS ON P.product_categories = HOME_DEPOT_POS.Prod_categ
		JOIN Retailers on Retailers.RetailerName = 'The Home Depot'
		JOIN Statements on Statements.Program_Id = @Period 
			AND Statements.ClientCode = P.ClientCode
		JOIN STR_Stores on STR_Stores.unit_number = HOME_DEPOT_POS.[STR_NBR] 
			AND STR_Stores.ClientCode = P.ClientCode AND STR_Stores.store_code = 'HD'
		JOIN RetailerInvoices on Retailers.Id = RetailerInvoices.RetailerId 
			AND RetailerInvoices.StatementId = Statements.Id
			 
		WHERE MONTH(HOME_DEPOT_POS.trs_dt) = @Period 
			and YEAR(HOME_DEPOT_POS.trs_dt)=2017		
		AND p.ClientCode IN (SELECT val FROM dbo.f_split(@ClientCodes,','))
		AND (LEN(HOME_DEPOT_POS.EnergyStar) > 5)
	) r

	select Account, AccountProvider, FullName, Email, ServiceAddress, ServiceCity,ServiceState,
	ServiceZip, Phone, CustomerNotes, CustomerProvider, CustomerInvoiceDescription, MeasureProductType, MeasureQuantity,MeasureBrand,
	MeasureModelNumber,MeasureSku,MeasureMef,MeasureCapacity,MeasureEnergyStarModelNumber,MeasureStoreName,MeasureLocation,RebateAccountID,
	RebatePayeeName,RebatePayeeAddress,RebatePayeeCity,RebatePayeeState,RebatePayeeZip,RebatePayeePhone,RebateStatus,RebateAmount,
	RebatePaymentNumber,RebateStatusDate,RebateApproved,RebateCommited,RebateCheckAmount,ApplicationStatusDate,ApplicationDescription,ApplicationReason from #TempSearsApiData
	UNION ALL 
	select Account, AccountProvider, FullName, Email, ServiceAddress, ServiceCity,ServiceState,
	ServiceZip, Phone, CustomerNotes, CustomerProvider, CustomerInvoiceDescription, MeasureProductType, MeasureQuantity,MeasureBrand,
	MeasureModelNumber,MeasureSku,MeasureMef,MeasureCapacity,MeasureEnergyStarModelNumber,MeasureStoreName,MeasureLocation,RebateAccountID,
	RebatePayeeName,RebatePayeeAddress,RebatePayeeCity,RebatePayeeState,RebatePayeeZip,RebatePayeePhone,RebateStatus,RebateAmount,
	RebatePaymentNumber,RebateStatusDate,RebateApproved,RebateCommited,RebateCheckAmount,ApplicationStatusDate,ApplicationDescription,ApplicationReason from #TempHomeApiData

	DROP TABLE #TempHomeApiData
	DROP TABLE #TempSearsApiData
END



GO


