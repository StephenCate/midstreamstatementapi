﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidstreamStatementService.Models
{
	public class StatementJob
	{
		public Guid Id { get; set; }
		public string ClientCodes { get; set; }
		public int Period { get; set; }
		public JobStatus Status { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime LastUpdated { get; set; }
	}
}
