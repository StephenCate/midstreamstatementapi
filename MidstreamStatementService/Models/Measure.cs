﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidstreamStatementService.Models
{
	public class Measure
	{
		public string ProductType { get; set; }
		public string MeasureID { get; set; }
		public double Quantity { get; set; }
		public List<Tuple<string,string>> MeasureAttributes { get; set; }
	}
}
