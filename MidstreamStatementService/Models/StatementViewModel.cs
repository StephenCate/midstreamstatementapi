﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidstreamStatementService.Models
{
	/// <summary>
	/// Encapsulates the bulk of the data that needs to be sent to HW
	/// </summary>
	public class StatementViewModel : IEqualityComparer<StatementViewModel>
	{
		public string Account { get; set; }
		public string AccountProvider { get; set; }
		public string FullName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
		public string ServiceAddress { get; set; }
		public string ServiceCity { get; set; }
		public string ServiceState { get; set; }
		public string ServiceZip { get; set; }
		public string MailingAddress { get; set; }
		public string MailingState { get; set; }
		public string MailingZip { get; set; }
		public string Phone { get; set; }
		public string UserID { get; set; }
		public string ApplicationID { get; set; }
		public string ResourceID { get; set; }
		
		public string CustomerNotes { get; set; }
		public string CustomerProvider { get; set; }
		public string CustomerInvoiceDescription { get; set; }
		public DateTime CustomerSalesStartDate { get; set; }
		public DateTime CustomerSalesEndDate { get; set; }

		public string MeasureID { get; set; }
		public string MeasureProductType { get; set; }
		public string MeasureMeasureID { get; set; }
		public double MeasureQuantity { get; set; }

		public string MeasureBrand { get; set; }
		public string MeasureModelNumber { get; set; }
		public string MeasureSku { get; set; }
		public string MeasureEnergyStarModelNumber { get; set; }
		public string MeasureStoreName { get; set; }
		public string MeasureLocation { get; set; }
		public string MeasureMef { get; set; }
		public string MeasureCapacity { get; set; }

		public string RebateAccountID { get; set; }
		public string RebatePayeeName { get; set; }
		public string RebatePayeeAddress { get; set; }
		public string RebatePayeeCity { get; set; }
		public string RebatePayeeState { get; set; }
		public string RebatePayeeZip { get; set; }
		public string RebatePayeePhone { get; set; }
		public string RebateStatus { get; set; }
		public double RebateAmount { get; set; }
		public DateTime RebateStatusDate { get; set; }
		public bool RebateApproved { get; set; }
		public bool RebateCommited { get; set; }
		public double RebateCheckAmount { get; set; }

		public string RebatePaymentType { get; set; }
		public string RebatePaymentNumber { get; set; }

		public DateTime ApplicationStatusDate { get; set; }
		public string ApplicationDescription { get; set; }
		public string ApplicationReason { get; set; }

		public bool Equals(StatementViewModel x, StatementViewModel y)
		{
			return (x.MeasureBrand.Equals(y.MeasureBrand) && x.MeasureEnergyStarModelNumber.Equals(y.MeasureEnergyStarModelNumber)
				&& x.MeasureModelNumber.Equals(y.MeasureModelNumber) && x.MeasureProductType.Equals(y.MeasureProductType)
				&& x.MeasureSku.Equals(y.MeasureSku));
		}

		public int GetHashCode(StatementViewModel obj)
		{
			return (obj.MeasureBrand + obj.MeasureEnergyStarModelNumber + obj.MeasureModelNumber + obj.MeasureProductType +
				obj.MeasureSku).GetHashCode();
		}
	}
}
