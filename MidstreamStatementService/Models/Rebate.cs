﻿using System;

namespace MidstreamStatementService.Models
{
	public class Rebate
	{
		public string AccountID { get; set; }
		public string PayeeName { get; set; }
		public string PayeeAddress { get; set; }
		public string PayeeCity { get; set; }
		public string PayeeState { get; set; }
		public string PayeeZip { get; set; }
		public string PaymentType { get; set; }
		public string PaymentNumber { get; set; }
		public string Status { get; set; }
		public double RebateAmount { get; set; }
		public DateTime StatusDate { get; set; }
		public bool Approved { get; set; }
		public bool Commited { get; set; }
		public double CheckAmount { get; set; }
	}
}