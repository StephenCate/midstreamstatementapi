﻿using System;

namespace MidstreamStatementService
{
	public static class BusinessUtility
	{
		public static string GenerateCode()
		{
			var returnVal = string.Empty;

			// Taken from the Rebate Center -  as is...
			string Codes = Guid.NewGuid().ToString().Replace("-", "");
			Codes = Codes.Substring(0, 4) + "-" + Codes.Substring(14, 6) + "-" + Codes.Substring(Codes.Length - 4);

			return Codes;
		}

		public static string GetSpecifiedLengthString(this Guid id, int length)
		{
			return id.ToString().Substring(0, length);
		}

		public static string GetSpecifiedLengthString(this string id, int length)
		{
			return id.Substring(0, length);
		}

		public static string GetShortReferenceString(this Guid id)
		{
			return "R" + id.ToString().Substring(0, 5);
		}

		public static string GetShortReferenceString(this string id)
		{
			return "R" + id.Substring(0, 5);
		}

		public static string GetReferenceIdString(this string id)
		{
			if (string.IsNullOrEmpty(id) || id.Length < 6)
				return id;
			return id.Substring(1, 5);
		}
	}
}