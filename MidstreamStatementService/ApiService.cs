﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidstreamStatementService
{
    public class ApiService
    {
		/// <summary>
		/// Makes the API call to HW to post the Statement for the current user and period
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="period"></param>
		/// <returns></returns>
		public bool PostStatementsForUserByPeriod(string userId, int period)
		{
			Console.WriteLine($"Post Statement for {userId} in the period {period}");

            return false;
		}
    }
}
