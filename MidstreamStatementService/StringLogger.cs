﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidstreamStatementService
{
	public class StringLogger
	{
		private ILogger _logger;
		private StringBuilder _sb;
		private string _newLine = "<br/>";
		public StringBuilder StringOutput
		{
			get
			{
				return _sb;
			}
			set
			{
				_sb = value;
			}
		}

		public StringLogger(ILogger logger)
		{
			_logger = logger;
			_sb = new StringBuilder();
		}

		public void WriteInfoLog(string message, params object[] param)
		{
			var stringParams = param.ToList().Select(c => c.ToString());
			WriteInfoLog(message, stringParams.ToArray());
		}

		private void WriteInfoLog(string message, params string[] param)
		{
			var formattedMessage = message;
			if (param.Length > 0)
			{
				formattedMessage = string.Format(message, param);
			}
			_logger.Info(formattedMessage);
			_sb.Append(formattedMessage + _newLine);
		}

		public void WriteErrorLog(Exception ex, string message, params string[] param)
		{


			var formattedMessage = message;
			if (param.Length > 0)
			{
				formattedMessage = string.Format(message, param);
			}
			_logger.Error(ex, formattedMessage);
			_sb.Append(formattedMessage + _newLine);
		}
	}
}
