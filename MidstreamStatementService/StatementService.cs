﻿using Dapper;
using MidstreamStatementService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Timers;

namespace MidstreamStatementService
{
	/// <summary>
	/// Polls the dbo.StatementJobs table to process any Statement that needs to be posted to
	///  HW via the Midstream API
	/// </summary>
	public class StatementService
	{
		private readonly Timer _timer;
		private StringLogger _logger;
		private const string dateFormat = "MM/dd/yyyy";

		private string NextAccountID = "FEE3NEXT";
		private string MidstreamResourceID = "D299662566";

		public StatementService()
		{
			_timer = new Timer(30000) { AutoReset = true };

			_timer.Elapsed += (sender, eventArgs) =>
			{
				_logger = new StringLogger(LogManager.GetLogger("MidstreamStatementService"));

				var countOfPendingJobs = ProcessPendingJobs();
			};
		}

		public void Start()
		{
			_timer.Start();
		}

		public void Stop()
		{
			_timer.Stop();
		}

		private int ProcessPendingJobs()
		{
			var countOfPendingJobs = 0;

			StringLogger currentInfoLog = null;
			using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MidStreamConnection"].ConnectionString))
			{
				var query = "select Id, ClientCodes, Period, Status from StatementJobs where Status = @Status";

				var pendingJobs = conn.Query<StatementJob>(query, new { Status = JobStatus.Pending }).ToList();

				if (pendingJobs.Count == 0)
				{
					_logger.WriteInfoLog("No PENDING jobs found in dbo.StatementJobs");
					return 0;
				}

				currentInfoLog = new StringLogger(LogManager.GetLogger("MidstreamStatementService"));

				_logger.WriteInfoLog($"Found {pendingJobs.Count} to run. Starting process...");
				currentInfoLog.WriteInfoLog($"Found {pendingJobs.Count} to run. Starting process...");

				foreach (var job in pendingJobs)
				{
					_logger.WriteInfoLog($"Start processing for Job ID: {job.Id} (ClientCodes: {job.ClientCodes}, Period: {job.Period})");
					currentInfoLog.WriteInfoLog($"Start processing for Job ID: {job.Id} (ClientCodes: {job.ClientCodes}, Period: {job.Period})");

					var updateQuery = "update StatementJobs set Status = @Status, LastUpdated = @LastUpdated where Id = @Id";
					try
					{
						var statementDetails = conn.Query<StatementViewModel>("sp_GetStatementDetailsForHwApi",
												new { ClientCodes = job.ClientCodes, Period = job.Period },
												null, false, null, System.Data.CommandType.StoredProcedure).ToList();

						conn.Query(updateQuery, new { Status = JobStatus.InProgress, Id = job.Id, LastUpdated = DateTime.Now });

						// We will be sending an API call per Model per Measure (or Product Type) per Provider Code per Retailer
						var retailerAccounts = statementDetails.Select(c => c.Account).Distinct();

						var batchNumber = 0; // Increment this whenever we make an API call for this Job

						foreach (var account in retailerAccounts) // Per Retailer
						{
							var providers = statementDetails.Where(c => c.Account == account)
															.Select(c => c.CustomerProvider).Distinct();

							foreach (var provider in providers) // Per Provider
							{
								var locations = statementDetails.Where(c => c.Account == account
																&& c.CustomerProvider == provider)
																.Select(c => c.MeasureLocation).Distinct();

								foreach (var storeLocation in locations) // Per Store Location
								{
									var measures = statementDetails.Where(c => c.Account == account
																	&& c.CustomerProvider == provider
																	&& c.MeasureLocation == storeLocation)
																.Select(c => c.MeasureProductType).Distinct();

									foreach (var measure in measures) // Per Measure
									{
										var energyStarModels = statementDetails.Where(c => c.Account == account
																				&& c.CustomerProvider == provider
																				&& c.MeasureLocation == storeLocation
																				&& c.MeasureProductType == measure)
																			.Select(c => c.MeasureEnergyStarModelNumber).Distinct();

										foreach (var model in energyStarModels) // Per Model
										{
											var statementDetailsToSend = statementDetails.Where(s => s.Account == account
																							&& s.CustomerProvider == provider
																							&& s.MeasureLocation == storeLocation
																							&& s.MeasureProductType == measure
																							&& s.MeasureEnergyStarModelNumber == model)
																						.Select(s => s).ToList();

											SendApiCallForRetailerSet($"{job.Id.GetSpecifiedLengthString(7)}{++batchNumber}", statementDetailsToSend);
										}
									}
								}
								
							}
						}

						_logger.WriteInfoLog($"Finished processing for Job ID: {job.Id} with {batchNumber} API calls (ClientCodes: {job.ClientCodes}, Period: {job.Period})");

						conn.Query(updateQuery, new { Status = JobStatus.Done, Id = job.Id, LastUpdated = DateTime.Now });
					}
					catch (Exception e)
					{
						conn.Query(updateQuery, new { Status = JobStatus.Error, Id = job.Id, LastUpdated = DateTime.Now });
						_logger.WriteErrorLog(e, $"Failed running Job ID: {job.Id} (ClientCodes: {job.ClientCodes}, Period: {job.Period})");
					}
				}

				SendBatchCompletionNotification(_logger.StringOutput);
			}
			return countOfPendingJobs;
		}

		private bool SendApiCallForRetailerSet(string jobId, List<StatementViewModel> statementDetailsForRetailer)
		{
			var result = false;

			try
			{
				var retailerInfo = statementDetailsForRetailer.FirstOrDefault();

				dynamic req = new ExpandoObject();

				req.account = retailerInfo.Account;
				req.accountProvider = retailerInfo.AccountProvider;
				req.name = retailerInfo.FullName;
                try
                {
                    req.lastname = retailerInfo.FullName.Split(' ').Last();
                }
                catch
                {
                    //ignore error
                }
				req.email = retailerInfo.Email;
				req.serviceAddress = retailerInfo.ServiceAddress;
				req.serviceCity = retailerInfo.ServiceCity;
				req.serviceState = retailerInfo.ServiceState;
				req.serviceZip = retailerInfo.ServiceZip;
				req.mailingAddress = retailerInfo.ServiceAddress;
				req.mailingCity = retailerInfo.ServiceCity;
				req.mailingState = retailerInfo.ServiceState;
				req.mailingZip = retailerInfo.ServiceZip;
				req.phone1 = retailerInfo.Phone;
				req.phone2 = "";
				req.userID = "NEXT";
				req.applicationID = jobId.Trim().Length > 10 ? jobId.Substring(0,9) : jobId;

				var providerId = retailerInfo.AccountProvider.Substring(0, 4);

				var customerAttributes = new List<dynamic>();

				customerAttributes.Add(new { name = "NOTES", value = retailerInfo.CustomerNotes });
				customerAttributes.Add(new { name = "PROVIDER", value = retailerInfo.CustomerProvider });
				customerAttributes.Add(new { name = "INVOICE DESCRIPTION", value = retailerInfo.CustomerInvoiceDescription });

				var startDate = new DateTime(retailerInfo.RebateStatusDate.Year,
					retailerInfo.RebateStatusDate.Month, 1);
				customerAttributes.Add(new { name = "SALES START DATE", value = startDate.ToString(dateFormat) }); // First of month

				var endDate = new DateTime(retailerInfo.RebateStatusDate.Year,
					retailerInfo.RebateStatusDate.Month,
					DateTime.DaysInMonth(retailerInfo.RebateStatusDate.Year, retailerInfo.RebateStatusDate.Month)); // End of month

				customerAttributes.Add(new { name = "SALES END DATE", value = endDate.ToString(dateFormat) });

				req.customerAttributes = customerAttributes;

				// All things the same except the ProductType-specific information
				// Quantity
				// Amounts
				// Measure
				var measures = new List<dynamic>();

				var productTypes = statementDetailsForRetailer.Select(c => c.MeasureProductType).Distinct().ToList();

				foreach (var productType in productTypes)
				{
					dynamic measure = new ExpandoObject();
					measure.productType = retailerInfo.MeasureProductType; // GetProductTypeCode(providerId, productType);
					measure.measureID = retailerInfo.MeasureID;
					measure.quantity = statementDetailsForRetailer
						.Where(s => s.MeasureProductType == productType)
						.Sum(s => s.MeasureQuantity);

					measure.measureAttributes = new List<dynamic>();

					// For each product type, we need to get the measure attributes
					var distinctMeasureAttributes = statementDetailsForRetailer
						.Where(s => s.MeasureProductType == productType).Select(c => c).Distinct().ToList();
					// MANUFACTURER/BRAND
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureBrand).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "MANUFACTURER/BRAND", value = attribute ?? "" });
					}

					// MODEL NUMBER
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureModelNumber).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "MODEL NUMBER", value = attribute ?? "" });
					}

					// SKU
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureSku).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "SKU", value = attribute ?? "" });
					}
					// ENERGY STAR MODEL NUMBER
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureEnergyStarModelNumber).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "ENERGY STAR MODEL NUMBER", value = attribute ?? "" });
					}
					// STORE NAME
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureStoreName).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "STORE NAME", value = attribute ?? "" });
					}
					// STORE LOCATION
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureLocation).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "STORE LOCATION", value = attribute ?? "" });
					}

					// MEF
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureMef).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "MEF", value = attribute ?? "" });
					}

					// CAPACITY
					foreach (var attribute in distinctMeasureAttributes.Select(c => c.MeasureCapacity).Distinct().ToList())
					{
						measure.measureAttributes.Add(new { name = "CAPACITY", value = attribute ?? "" });
					}

					measures.Add(measure);
				}
				req.measures = measures;

				var rebates = new List<dynamic>();
				var rebateAmountTotal = statementDetailsForRetailer.Sum(c => c.RebateAmount);
				rebates.Add(new
				{
					accountID = retailerInfo.RebateAccountID,
					payeeName = retailerInfo.RebatePayeeName,
					payeeAddress = retailerInfo.ServiceAddress,
					payeeCity = retailerInfo.ServiceCity,
					payeeState = retailerInfo.ServiceState,
					payeeZip = retailerInfo.ServiceZip,
					paymentType = "CHECK",
					paymentNumber = retailerInfo.RebatePaymentNumber,
					status = "PAID",
					rebateAmount = rebateAmountTotal,
					statusDate = retailerInfo.RebateStatusDate.ToString(dateFormat),
					approved = "1",
					commited = "1",
					checkAmount = rebateAmountTotal
				});

				req.rebates = rebates;
				req.interactions = null;
				req.applicationStatus = new
				{
					date = retailerInfo.RebateStatusDate.ToString(dateFormat),
					description = "Complete",
					reason = "Application Complete"
				};
				req.images = new List<dynamic>();

				var baseUrl = ConfigurationManager.AppSettings["HwBaseUrlForMidstreamPost"];
				var resourceUrl = ConfigurationManager.AppSettings["HwResourceUrlForMidstreamPost"];

				resourceUrl = string.Format(resourceUrl, retailerInfo.ResourceID);

				_logger.WriteInfoLog("MIDSTREAM API Call | Start | SendApiCallForRetailerSet (claim ID : {0}) | Request URL: {1}{2}",
						jobId, baseUrl, resourceUrl);

				RestClient client = new RestClient(baseUrl);
				RestRequest request = new RestRequest(resourceUrl, Method.POST);
				request.AddHeader("Accept", "application/json");

				JObject requestObject = JObject.FromObject(req); // Needed to pre-parse the req ExpandoObject as this was throwing Stack Overflow exceptions
				request.RequestFormat = DataFormat.Json;
				request.AddBody(req);

				_logger.WriteInfoLog("MIDSTREAM API Call | Request | SendApiCallForRetailerSet (Job ID : {0}) | Request Body: {1}",
					jobId, JsonConvert.SerializeObject(requestObject));

				var response = client.Execute<object>(request);

				_logger.WriteInfoLog("MIDSTREAM API Call | Result | SendApiCallForRetailerSet (Job ID : {0}) | StatusCode: {1}",
						jobId, response.StatusCode.ToString());

				var responseObject = JObject.FromObject(response.Data);

				_logger.WriteInfoLog("MIDSTREAM API Call | Result | SendApiCallForRetailerSet (Job ID : {0}) | Response Body: {1}",
					jobId, JsonConvert.SerializeObject(responseObject));

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					result = true;
				}
			}
			catch (Exception e)
			{
				_logger.WriteErrorLog(e, "Error running the Midstream Details to HW for Job ID: {0}", jobId);
				throw e;
			}
			return result;
		}

		private string GetProductTypeCode(string providerId, string productType)
		{
			var productTypeCode = string.Empty;

			switch (productType)
			{
				case "DH":
					productTypeCode = $"{providerId}DHMF17";
                    break;

				case "AC":
					productTypeCode = $"{providerId}RMAC17";
					break;

				case "AP":
					productTypeCode = $"{providerId}AIRPRF";
					break;

				case "CD":
					productTypeCode = $"{providerId}CLDR17";
					break;

				case "CW":
					productTypeCode = $"{providerId}CLWS17";
					break;

				case "Dishwasher":
					productTypeCode = "";
					break;

				case "Freezer":
				case "Fridge":
					productTypeCode = $"{providerId}FRZR17";
					break;
			}

			return productTypeCode;
		}

		private void SendBatchCompletionNotification(StringBuilder log)
		{
			string from = "notification@nxtpay.com";
			string subject = string.Format("[MIDSTREAM Notification] Completed Application API Logs - {0} {1} - {2}",
				DateTime.Now.ToString("yyyyMMdd"),
				DateTime.Now.ToString("HHmmssfff"),
				Environment.MachineName);

			var message = new MailMessage();
			var toAddressList = ConfigurationManager.AppSettings["MidstreamNotificationsList.CompletedApi"];
			foreach (var s in toAddressList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
			{
				message.To.Add(new MailAddress(s));
			}

			message.From = new MailAddress(from);
			message.Body = log.ToString();
			message.IsBodyHtml = true;
			message.Subject = subject;
			SendAdministratorEmail(message);
		}

		private void SendAdministratorEmail(MailMessage message)
		{
			message.IsBodyHtml = true;
			message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message.Body, null, MediaTypeNames.Text.Html));

			SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"].ToString(),
				Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"].ToString()));
			NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUserName"],
				ConfigurationManager.AppSettings["SmtpPassword"].ToString());
			smtpClient.Credentials = credentials;
			smtpClient.EnableSsl = true;
			smtpClient.Send(message);
		}
	}

	public enum JobStatus
	{
		Pending = 1,
		InProgress = 2,
		Error = 3,
		Done = 4
	}
}