﻿using System;
using System.Timers;
using Topshelf;

namespace MidstreamStatementService.Host
{
	public class TownCrier
	{
		private readonly Timer _timer;

		public TownCrier()
		{
			_timer = new Timer(1000) { AutoReset = true };
			_timer.Elapsed += (sender, eventArgs) => Console.WriteLine("It is {0} and all is well", DateTime.Now);
		}

		public void Start()
		{
			_timer.Start();
		}

		public void Stop()
		{
			_timer.Stop();
		}
	}

	internal class Program
	{
		private static void Main(string[] args)
		{
			HostFactory.Run(x =>
			{
				x.Service<StatementService>(s =>
				{
					s.ConstructUsing(name => new StatementService());
					s.WhenStarted(ss => ss.Start());
					s.WhenStopped(ss => ss.Stop());
				});
				x.RunAsLocalSystem();

				x.SetDescription("Sends Midstream Statements to HW via the Completed API.");
				x.SetDisplayName("MidstreamStatementService");
				x.SetServiceName("MidstreamStatementService");
			});
		}
	}
}